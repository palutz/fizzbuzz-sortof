# Fizzbuzz #
(branch from step1 to step3 to see the evolution)

### Pre-requisites ###

* Java JDK (Oracle or OpenJDK) 1.8.x
* Scala 2.11.8+ (compiled with 2.12.x) (http://www.scala-lang.org/) 
* sbt 0.13.x (http://www.scala-sbt.org/)

### How do I get set up? ###

* From the project root just run `$ sbt`
* '> update` or`compile` if you want to compile before running
* `> run 1 20 (or any other interval you want to insert)
* CTRL+D to exit the sbt
