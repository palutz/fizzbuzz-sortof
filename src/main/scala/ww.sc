
def fizzBuzz1(i : Int) : String = {
  i match {
    case a if (a % 5 == 0) && (a % 3 == 0) => "fizzbuzz"
    case a if a.toString.contains('3') => "lucky"
    case a if a % 5 == 0 => "fizz"
    case a if a % 3 == 0 => "buzz"
    case a => a.toString
  }
}

def fizzBuzz2(i : Int) : String = {
  i match {
    case a if (a % 5 == 0) && (a % 3 == 0) => "fizzbuzz"
    case a if a.toString.contains('3') => "lucky"
    case a if a % 5 == 0 => "fizz"
    case a if a % 3 == 0 => "buzz"
    case a => a.toString
  }
}

def printer(v : List[String]): String = {
  v.mkString(" ")
}

def step2(x: Int, y: Int)(implicit f: Int => String, implicit printOut: List[String] => String): Unit = {
  val v = (for {
    a <- (x to y)
  } yield f(a)).toList
  printOut(v)
}

implicit val p = printer(_)
step2(1, 20)